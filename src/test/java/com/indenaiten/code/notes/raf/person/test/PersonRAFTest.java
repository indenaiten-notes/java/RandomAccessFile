package com.indenaiten.code.notes.raf.person.test;

//IMPORTS
import com.indenaiten.code.notes.raf.person.PersonRAF;
import com.indenaiten.code.notes.raf.person.model.Person;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

/**
 * Clase que prueba la funcionalidad de la clase "PersonRAF".
 */
@TestMethodOrder( OrderAnnotation.class )
public class PersonRAFTest {
    /** Lista con Personas para realizar las pruebas. */
    private List<Person> personList;

    /** Clase que vamos a utilizar para realizar las pruebas. */
    private PersonRAF personRAF = new PersonRAF();

    /**
     * Método que se ejecuta al principio de las pruebas.
     * Vamos a utlizar este método para inicializar los datos de prueba.
     */
    @BeforeEach
    void setUp() {
        this.personList = new ArrayList<>();
        this.personList.add( new Person(1L, "15424877-A", "Patricia", "Fernández", "González", 27, 'M', true ) );
        this.personList.add( new Person(2L, "64715491-A", "Ángel", "Herce", "Soto", 28, 'V', true ) );
        this.personList.add( new Person(3L, "64812116-F", "Sara", "Rodriguez", "González", 35, 'M', false ) );
        this.personList.add( new Person(4L, "84995745-L", "Melani", "González", "Soto", 21, 'M', false ) );
        this.personList.add( new Person(5L, "61248854-H", "Paco", "Soto", "Garcia", 42, 'V', false ) );
    }

    /**
     * Test que se va a ejecutar en primer lugar.
     * Prueba que se almacenen toda la información de las personas que tenemos de prueba.
     *
     * @throws Exception
     */
    @Test
    @Order( 1 )
    void writeTest() throws Exception {
        //Escribimos en un fichero ".dat" todos los objetos "Person" mediante la clase "PersonRAF".
        this.personRAF.save( this.personList );

        //Comprobamos que exista el fichero ".dat".
        assertTrue( PersonRAF.FILE.exists(),
                () -> String.format( "No existe el fichero \"%s\"", PersonRAF.FILE.getName() ) );

        //Comprobamos que el fichero ".dat" tenga bytes.
        assertTrue( PersonRAF.FILE.length() > 0,
                () -> String.format( "El fichero \"%s\" debe tener bytes.", PersonRAF.FILE ) );
    }

    /**
     * Test que se va a ejecutar en segundo lugar.
     * Prueba que se leee corretamente la información almacenada en el fichero ".dat" mediante la clase "PersonRAF".
     *
     * @throws Exception
     */
    @Test
    @Order( 2 )
    void readAllTest() throws Exception {
        //Leemos toda la información y la recuperamos como una lista de objetos de tipo "Person".
        List<Person> result = this.personRAF.readAll();

        //Comprobamos que el resultado no sea una lista vacía.
        assertFalse( result.isEmpty(),
                () -> "El resultado no puede ser una lista vacía." );

        //Comprobamos que el resultado contenga los mismos datos que los que escribimos.
        assertEquals( this.personList, result,
                () -> "El resultado no contiene los datos que se esperaban." );
    }

    /**
     * Test que se va a ejecutar en tercer lugar.
     * Prueba que elimina todos los datos del fichero ".dat" mediante la clase "PersonRAF".
     *
     * @throws Exception
     */
    @Test
    @Order( 3 )
    void clearTest() throws Exception {
        //Borramos la información del fichero ".dat".
        this.personRAF.clear();

        //Leemos toda la información y la recuperamos como una lista de objetos de tipo "Person".
        List<Person> result = this.personRAF.readAll();

        //Comprobamos que el resultado sea una lista vacía.
        assertTrue( result.isEmpty(),
                () -> "El resultado debe ser una lista vacía." );

        //Comprobamos que el tamaño en bytes del fichero ".dat" sea 0.
        assertEquals( PersonRAF.FILE.length(), 0,
                () -> String.format( "El fichero \"%s\" debe tener 0 bytes de tamaño.", PersonRAF.FILE ) );
    }
}