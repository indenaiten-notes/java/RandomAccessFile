package com.indenaiten.code.notes.raf.person.model;

//IMPORTS
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * POJO que no sirve para almacenar la información referente a un persona.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person implements Serializable {
    /** El ID que tendrá la persona. */
    private long id;

    /** El DNI que tiene la persona. */
    private String dni;

    /** El nombre que tiene la persona. */
    private String name;

    /** El primer apellido que tiene la persona. */
    private String lastname1;

    /** El segundo apellido que tiene la persona. */
    private String lastname2;

    /** La edad que tiene la persona. */
    private int age;

    /** La letra del sexo que tiene la persona. */
    private char sex;

    /** Campo que indica si la persona es premium o no. */
    private boolean isPremium;
}
