package com.indenaiten.code.notes.raf.person;

//IMPORTS

import com.indenaiten.code.notes.raf.person.model.Person;
import lombok.NoArgsConstructor;

import java.io.*;
import java.util.Collections;
import java.util.List;

/**
 * Clase que nos permite escribir la información de la clase "Person" en un fichero ".dat" y poder leer dicha información
 * de manera aleatoria.
 */
@NoArgsConstructor
public class PersonRAF{
    /** El fichero ".dat" en el que se almacena la información de la clase "Person". */
    public static final File FILE = new File("src/main/resources/person.dat" );

    /** La clase que nos permite leer el fichero ".dat" de manera aleatoria. */
    private RandomAccessFile raf;

    /**
     * Método que escribe en el fichero ".dat" la información de la lista de objetos de tipo "Person".
     *
     * @param listPersons La lista de objetos de tipo "Person" que se va a guardar en el fichero ".dat".
     * @throws Exception
     */
    public void save( final List<Person> listPersons ) throws Exception {
        //Abrimos el fichero ".dat" para leer y escribir en él ("rw").
        this.raf = new RandomAccessFile( FILE, "rw" );
        this.raf.seek( this.raf.length() );

        //Convertimos la lista de objetos de tipo "Person" a bytes.
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream( byteArrayOutputStream );
        objectOutputStream.writeObject( listPersons );
        final byte[] data = byteArrayOutputStream.toByteArray();

        //Escribimos en el fichero los bytes de la lista de objetos de tipo "Person".
        this.raf.write( data );

        //Cerramos el fichero ".dat"
        this.raf.close();
    }

    /**
     * Método que lee todos los registros del fichero ".dat".
     *
     * @return La lista con los registros del fichero ".dat" mapeados a la clase "Person".
     * @throws Exception
     */
    public List<Person> readAll() throws Exception {
        //Inicializamos el resultado que vamos a devolver al finalizar.
        List<Person> result = Collections.emptyList();

        //Abrimos el fichero ".dat" para leerlo ("r").
        this.raf = new RandomAccessFile( FILE, "r" );

        //Leemos toda la información del fichero ".dat".
        final byte[] data = new byte[ (int) this.raf.length() ];
        this.raf.readFully( data );
        this.raf.close();

        //Comprobamos si había información en el fichero ".dat".
        if( data.length > 0 ) { //Si hay información en el fichero ".dat".
            //Convertimos los bytes a una lista de objetos de tipo "Person" y la almacenamos en el la variable "result".
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream( data );
            ObjectInputStream readFile = new ObjectInputStream( byteArrayInputStream );
            result = (List<Person>) readFile.readObject();
        }

        //Devolvemos el resultado.
        return result;
    }

    /**
     * Método que elimina toda la información del fichero ".dat".
     *
     * @throws Exception
     */
    public void clear() throws Exception {
        //Abrimos el fichero ".dat" para leer y escribir en él ("rw").
        this.raf = new RandomAccessFile( FILE, "rw" );

        //Establecemos la longitud del archivo en 0 bytes para eliminar el contenido del fichero ".dat".
        this.raf.setLength( 0 );

        //Cerramos el fichero ".dat"
        this.raf.close();
    }
}