# CHANGELOG
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## UNRELEASED

---

## `v2.0.0` _07/12/2021_

### CHANGED
- Se ha actualizado el nombre de las clase `PersonRandomAccessFileTest` a `PersonRAFTest`.
- Se ha actualizado la clase `PersonRAFTest` para probar los nuevos cambios de la clase `PersonRAF`.
- Se ha actualizado la clase `PersonRAF` para escribir y leer una lista de objetos de tipo `Person` de una vez.
- Se ha actualizado el POJO `Person` para poder serializarlo.

---

## `v1.0.0` _28/11/2021_

### ADDED
- Se han creado los test necesarios para comprobar el funcionamiento de la clase `PersonRAF`.
  

- Se ha creado un ejemplo de escritura y lectura de con la clase `RandomAccessFile.`
  - Se ha creado el POJO `Person` que nos servirá para almacenar la información referente a una persona.
  - Se ha creado la clase `PersonRAF` que nos servirá para escribir y leer objetos de tipo `Person` en un fichero `.dat` mediante la clase `RandomAccessFile`.  
  

- Se han añadido las dependencias de _"Lombok"_ y _"JUnit Jupiter API"_ al fichero `pom.xml` de MAVEN.

---